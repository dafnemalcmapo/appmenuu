package com.mv.appmenu

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.bumptech.glide.Glide
import com.mv.appmenu.database.Alumno
import com.mv.appmenu.database.dbAlumnos

private const val PICK_IMAGE_REQUEST = 1
private const val REQUEST_PERMISSION_READ_EXTERNAL_STORAGE = 2

class DbFragment : Fragment() {
    private lateinit var btnGuardar: Button
    private lateinit var btnBuscar: Button
    private lateinit var btnBorrar : Button
    private lateinit var btnSeleccionarImagen : Button
    private lateinit var txtMatricula: EditText
    private lateinit var txtNombre: EditText
    private lateinit var txtDomicilio: EditText
    private lateinit var txtEspecialidad: EditText
    private lateinit var txtUrlImagen: TextView
    private lateinit var imgAlumno : ImageView
    private lateinit var db: dbAlumnos
    private var imageUri: Uri? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_db, container, false)

        btnGuardar = view.findViewById(R.id.btnGuardar)
        btnBuscar = view.findViewById(R.id.btnBuscar)
        btnBorrar = view.findViewById(R.id.btnBorrar)
        btnSeleccionarImagen = view.findViewById(R.id.btnSeleccionarImagen)
        txtMatricula = view.findViewById(R.id.txtMatricula)
        txtNombre = view.findViewById(R.id.txtNombre)
        txtDomicilio = view.findViewById(R.id.txtDomicilio)
        txtEspecialidad = view.findViewById(R.id.txtEspecialidad)
        txtUrlImagen = view.findViewById(R.id.lblUrlImagen)
        imgAlumno = view.findViewById(R.id.imgAlumno)

        btnSeleccionarImagen.setOnClickListener {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
                if (ContextCompat.checkSelfPermission(
                        requireContext(),
                        Manifest.permission.READ_MEDIA_IMAGES
                    ) != PackageManager.PERMISSION_GRANTED
                ) {
                    ActivityCompat.requestPermissions(
                        requireActivity(),
                        arrayOf(Manifest.permission.READ_MEDIA_IMAGES),
                        REQUEST_PERMISSION_READ_EXTERNAL_STORAGE
                    )
                } else {
                    openImageChooser()
                }
            } else {
                if (ContextCompat.checkSelfPermission(
                        requireContext(),
                        Manifest.permission.READ_EXTERNAL_STORAGE
                    ) != PackageManager.PERMISSION_GRANTED
                ) {
                    ActivityCompat.requestPermissions(
                        requireActivity(),
                        arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE),
                        REQUEST_PERMISSION_READ_EXTERNAL_STORAGE
                    )
                } else {
                    openImageChooser()
                }
            }
        }

        arguments?.let {
            val alumnoLista = it.getSerializable("mialumno") as AlumnoLista

            txtNombre.setText(alumnoLista.nombre)
            txtDomicilio.setText(alumnoLista.domicilio)
            txtEspecialidad.setText(alumnoLista.especialidad)
            txtMatricula.setText(alumnoLista.matricula)
            txtUrlImagen.setText(alumnoLista.foto)
            Glide.with(this)
                .load(alumnoLista.foto)
                .circleCrop()
                .placeholder(R.mipmap.foto)
                .error(R.mipmap.foto)
                .into(imgAlumno)
        }

        db = dbAlumnos(requireContext())
        db.openDataBase()

        btnGuardar.setOnClickListener {
            if (txtNombre.text.isNullOrEmpty() ||
                txtDomicilio.text.isNullOrEmpty() ||
                txtEspecialidad.text.isNullOrEmpty() ||
                txtMatricula.text.isNullOrEmpty()
            ) {
                Toast.makeText(requireContext(), "Falta información por capturar", Toast.LENGTH_SHORT).show()
            } else {
                val matricula = txtMatricula.text.toString()
                val alumnoExistente = db.getAlumno(matricula)
                if (alumnoExistente.matricula.isNotEmpty()) {
                    alumnoExistente.apply {
                        nombre = txtNombre.text.toString()
                        domicilio = txtDomicilio.text.toString()
                        especialidad = txtEspecialidad.text.toString()
                        foto = imageUri?.toString() ?: foto
                    }

                    db.actualizarAlumno(alumnoExistente)
                    Toast.makeText(requireContext(), "Se actualizó el alumno con matrícula $matricula", Toast.LENGTH_SHORT).show()
                } else {
                    val alumno = Alumno(
                        matricula = txtMatricula.text.toString(),
                        nombre = txtNombre.text.toString(),
                        domicilio = txtDomicilio.text.toString(),
                        especialidad = txtEspecialidad.text.toString(),
                        foto = imageUri?.toString() ?: "Pendiente"
                    )
                    val id: Long = db.insertarAlumno(alumno)
                    Toast.makeText(requireContext(), "Se agregó el alumno con ID $id", Toast.LENGTH_SHORT).show()
                }
                limpiarCampos()
            }
        }

        btnBuscar.setOnClickListener {
            if (txtMatricula.text.toString().isEmpty()) {
                Toast.makeText(requireContext(), "Falta capturar matrícula", Toast.LENGTH_SHORT).show()
            } else {
                val alumno = db.getAlumno(txtMatricula.text.toString())
                if (alumno.matricula.isNotEmpty()) {
                    txtNombre.setText(alumno.nombre)
                    txtDomicilio.setText(alumno.domicilio)
                    txtEspecialidad.setText(alumno.especialidad)
                    txtUrlImagen.text = alumno.foto
                    Glide.with(this)
                        .load(alumno.foto)
                        .circleCrop()
                        .placeholder(R.mipmap.foto)
                        .error(R.mipmap.foto)
                        .into(imgAlumno)
                } else {
                    Toast.makeText(requireContext(), "No se encontró la matrícula", Toast.LENGTH_SHORT).show()
                }
            }
        }

        btnBorrar.setOnClickListener {
            if (txtMatricula.text.toString().isEmpty()) {
                Toast.makeText(requireContext(), "Falta capturar matrícula", Toast.LENGTH_SHORT).show()
            } else {
                val matricula = txtMatricula.text.toString()
                AlertDialog.Builder(requireContext())
                    .setTitle("Confirmar eliminación")
                    .setMessage("¿Está seguro de que desea eliminar el alumno con matrícula $matricula?")
                    .setPositiveButton("Sí") { dialog, which ->
                        val deletedRows = db.borrarAlumno(matricula)
                        if (deletedRows > 0) {
                            Toast.makeText(requireContext(), "Se eliminó el alumno con matrícula $matricula", Toast.LENGTH_SHORT).show()
                            limpiarCampos()
                        } else {
                            Toast.makeText(requireContext(), "No se encontró la matrícula", Toast.LENGTH_SHORT).show()
                        }
                    }
                    .setNegativeButton("No", null)
                    .show()
            }
        }

        return view
    }

    private fun openImageChooser() {
        val intent = Intent(Intent.ACTION_PICK)
        intent.type = "image/*"
        startActivityForResult(intent, PICK_IMAGE_REQUEST)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == PICK_IMAGE_REQUEST && resultCode == Activity.RESULT_OK && data != null && data.data != null) {
            imageUri = data.data
            Glide.with(this)
                .load(imageUri)
                .circleCrop()
                .placeholder(R.mipmap.foto)
                .error(R.mipmap.foto)
                .into(imgAlumno)
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == REQUEST_PERMISSION_READ_EXTERNAL_STORAGE) {
            if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                openImageChooser()
            } else {
                Toast.makeText(requireContext(), "Permiso denegado", Toast.LENGTH_SHORT).show()
            }
        }
    }

    private fun limpiarCampos() {
        txtMatricula.setText("")
        txtNombre.setText("")
        txtDomicilio.setText("")
        txtEspecialidad.setText("")
        txtUrlImagen.text = ""
        imgAlumno.setImageResource(R.mipmap.foto)
    }
}
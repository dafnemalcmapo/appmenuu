package com.mv.appmenu

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.SearchView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.mv.appmenu.database.dbAlumnos

class ExitFragment : Fragment() {

    private lateinit var rcvLista: RecyclerView
    private lateinit var adaptador: MiAdaptador
    private lateinit var btnNuevo: FloatingActionButton
    private lateinit var searchView: SearchView
    private lateinit var listaAlumno: ArrayList<AlumnoLista>
    private lateinit var filteredList: ArrayList<AlumnoLista>
    private lateinit var db: dbAlumnos

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_exit, container, false)
        rcvLista = view.findViewById(R.id.recId)
        searchView = view.findViewById(R.id.menu_search)
        btnNuevo = view.findViewById(R.id.agregarAlumno)
        rcvLista.layoutManager = LinearLayoutManager(requireContext())

        listaAlumno = ArrayList() // Inicialización de listaAlumno
        filteredList = ArrayList() // Inicialización de filteredList
        adaptador = MiAdaptador(filteredList, requireContext())
        rcvLista.adapter = adaptador

        btnNuevo.setOnClickListener {
            cambiarDBFragment()
        }

        adaptador.setOnClickListener {
            val pos: Int = rcvLista.getChildAdapterPosition(it)
            val alumno: AlumnoLista = listaAlumno[pos]
            val bundle = Bundle().apply {
                putSerializable("mialumno", alumno)
            }
            val dbFragment = DbFragment()
            dbFragment.arguments = bundle

            parentFragmentManager.beginTransaction()
                .replace(R.id.frmContenedor, dbFragment)
                .addToBackStack(null)
                .commit()
        }

        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                query?.let { filtrarAlumnos(it) }
                return true
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                newText?.let { filtrarAlumnos(it) }
                return true
            }
        })

        db = dbAlumnos(requireContext()) // Inicialización del objeto db
        cargarLista()

        return view
    }

    private fun cargarLista() {
        listaAlumno.clear()
        db.openDataBase()
        val cursor = db.getAllAlumnos()
        if (cursor.moveToFirst()) {
            do {
                val alumno = AlumnoLista(
                    cursor.getInt(0),
                    cursor.getString(1),
                    cursor.getString(2),
                    cursor.getString(3),
                    cursor.getString(4),
                    cursor.getString(5)
                )
                listaAlumno.add(alumno)
            } while (cursor.moveToNext())
        }
        cursor.close()
        db.close()
        adaptador.actualizarLista(listaAlumno) // Actualizar lista en el adaptador
    }

    private fun filtrarAlumnos(texto: String) {
        val listaFiltrada = listaAlumno.filter { alumno ->
            alumno.nombre.contains(texto, ignoreCase = true) ||
                    alumno.domicilio.contains(texto, ignoreCase = true)
        }
        adaptador.actualizarLista(listaFiltrada)
    }

    private fun cambiarDBFragment() {
        val cambioFragment = fragmentManager?.beginTransaction()
        cambioFragment?.replace(R.id.frmContenedor, DbFragment())
        cambioFragment?.addToBackStack(null)
        cambioFragment?.commit()
    }

    fun actualizarLista() {
        cargarLista()
    }

    override fun onDestroy() {
        db.close() // Cerrar la base de datos cuando se destruya el fragmento
        super.onDestroy()
    }
}

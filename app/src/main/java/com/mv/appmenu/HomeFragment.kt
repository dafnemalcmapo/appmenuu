package com.mv.appmenu

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.Fragment

class HomeFragment : Fragment() {
    private lateinit var imgAlumno: ImageView
    private lateinit var txtNombre: TextView
    private lateinit var txtMateria: TextView
    private lateinit var txtCarrera: TextView
    private lateinit var txtCorreo: TextView

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_home, container, false)

        imgAlumno = view.findViewById(R.id.imgAlumno)
        txtNombre = view.findViewById(R.id.txtNombre)
        txtMateria = view.findViewById(R.id.txtMateria)
        txtCarrera = view.findViewById(R.id.txtCarrera)

        txtNombre.text = "Dafne Jaqueline Malcampo Virgen"
        txtMateria.text = "Programación Móvil"
        txtCarrera.text = "Ingeniería en Tecnologías de la Información"

        return view
    }
}
package com.mv.appmenu

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.Filter
import android.widget.ListView
import androidx.appcompat.widget.SearchView
import androidx.fragment.app.Fragment

class ListFragment : Fragment() {

    private lateinit var listView: ListView
    private lateinit var searchView: SearchView
    private lateinit var arrayList: ArrayList<String>
    private lateinit var adapter: ArrayAdapter<String>

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_list, container, false)

        listView = view.findViewById(R.id.lstAlumnos)
        searchView = view.findViewById(R.id.menu_search)

        val items = resources.getStringArray(R.array.alumnos)
        arrayList = ArrayList()
        arrayList.addAll(items.map { it.trim() })

        adapter = object : ArrayAdapter<String>(requireContext(), android.R.layout.simple_list_item_1, arrayList) {
            private var filteredItems: ArrayList<String> = arrayList

            override fun getCount(): Int {
                return filteredItems.size
            }

            override fun getItem(position: Int): String? {
                return filteredItems[position]
            }

            override fun getFilter(): Filter {
                return object : Filter() {
                    override fun performFiltering(constraint: CharSequence?): FilterResults {
                        val charString = constraint?.toString() ?: ""
                        filteredItems = if (charString.isEmpty()) {
                            arrayList
                        } else {
                            val filteredList = arrayList.filter {
                                it.contains(charString, ignoreCase = true)
                            } as ArrayList<String>
                            filteredList
                        }
                        return FilterResults().apply { values = filteredItems }
                    }

                    override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
                        filteredItems = if (results?.values == null) {
                            arrayList
                        } else {
                            results.values as ArrayList<String>
                        }
                        notifyDataSetChanged()
                    }
                }
            }
        }

        listView.adapter = adapter

        // Configurar el SearchView
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                adapter.filter.filter(newText)
                return false
            }
        })

        return view
    }
}

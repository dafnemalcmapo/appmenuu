package com.mv.appmenu.database

import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import androidx.core.content.contentValuesOf

class dbAlumnos(private val context: Context) {
    private val dbHelper: AlumnosDbHelper = AlumnosDbHelper(context)
    private lateinit var db: SQLiteDatabase

    private val leerCampos = arrayOf(
        DefinirTabla.Alumnos.ID,
        DefinirTabla.Alumnos.MATRICULA,
        DefinirTabla.Alumnos.NOMBRE,
        DefinirTabla.Alumnos.DOMICILIO,
        DefinirTabla.Alumnos.ESPECIALIDAD,
        DefinirTabla.Alumnos.FOTO
    )

    fun openDataBase() {
        db = dbHelper.writableDatabase
    }

    fun insertarAlumno(alumno: Alumno): Long {
        val valores = contentValuesOf().apply {
            put(DefinirTabla.Alumnos.MATRICULA, alumno.matricula)
            put(DefinirTabla.Alumnos.NOMBRE, alumno.nombre)
            put(DefinirTabla.Alumnos.DOMICILIO, alumno.domicilio)
            put(DefinirTabla.Alumnos.ESPECIALIDAD, alumno.especialidad)
            put(DefinirTabla.Alumnos.FOTO, alumno.foto)
        }
        return db.insertWithOnConflict(DefinirTabla.Alumnos.TABLA, null, valores, SQLiteDatabase.CONFLICT_IGNORE)
    }

    fun actualizarAlumno(alumno: Alumno): Int {
        val valores = contentValuesOf().apply {
            put(DefinirTabla.Alumnos.MATRICULA, alumno.matricula)
            put(DefinirTabla.Alumnos.NOMBRE, alumno.nombre)
            put(DefinirTabla.Alumnos.DOMICILIO, alumno.domicilio)
            put(DefinirTabla.Alumnos.ESPECIALIDAD, alumno.especialidad)
            put(DefinirTabla.Alumnos.FOTO, alumno.foto)
        }
        return db.update(DefinirTabla.Alumnos.TABLA, valores,
            "${DefinirTabla.Alumnos.MATRICULA} = ?", arrayOf(alumno.matricula))
    }

    fun borrarAlumno(matricula: String): Int {
        return db.delete(DefinirTabla.Alumnos.TABLA, "${DefinirTabla.Alumnos.MATRICULA} = ?", arrayOf(matricula))
    }

    fun mostrarAlumnos(cursor: Cursor): Alumno {
        return Alumno().apply {
            id = cursor.getInt(0)
            matricula = cursor.getString(1)
            nombre = cursor.getString(2)
            domicilio = cursor.getString(3)
            especialidad = cursor.getString(4)
            foto = cursor.getString(5)
        }
    }

    fun getAlumno(matricula: String): Alumno {
        val db = dbHelper.readableDatabase
        val cursor = db.query(DefinirTabla.Alumnos.TABLA, leerCampos, "${DefinirTabla.Alumnos.MATRICULA} = ?", arrayOf(matricula), null, null, null)
        return if (cursor.moveToFirst()) {
            val alumno = mostrarAlumnos(cursor)
            cursor.close()
            alumno
        } else {
            cursor.close()
            Alumno() // Devuelve un alumno vacío si no se encuentra
        }
    }
    fun getAllAlumnos(): Cursor {
        val db = dbHelper.readableDatabase
        return db.query(DefinirTabla.Alumnos.TABLA, leerCampos, null, null, null, null, null)
    }


    fun close() {
        dbHelper.close()
    }
}

